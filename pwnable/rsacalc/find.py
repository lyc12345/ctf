import sympy,sys,fractions,string

gcd = fractions.gcd
invert = sympy.invert
c = int(sys.argv[1])
"""
a^e mod n == c
n = p*q
phi = (p-1)*(q-1)
gcd(e,phi) = 1
"""

for a in string.ascii_letters+string.digits:
    for e in xrange(3,10):
        b = ord(a)**e
        b -= c
        if b<0:
            continue
        while b%2 == 0:
            b/=2
        ok = False
        p = 0
        for i in xrange(32767,2,-1):
            if b%i==0 and (b/i)<32768:
                ok = True
                p = i
                q = b/i
                break
        if ok:
            phi = (p-1)*(q-1)
            ok = False
            if gcd(phi,e) == 1:
                ok = True
                print a,p,q,e,invert(e,phi)
        if ok:
            raw_input('@')
