#include<bits/stdc++.h>
#define X first
#define Y second
using namespace std;


char t[10][10];
int k,n;
vector<pair<int,int> >s;
bool row[10][10],col[10][10],block[10][10];
bool ok;
int type;
void parse(){
    memset(row,0,sizeof(row));
    memset(block,0,sizeof(block));
    memset(col,0,sizeof(col));
    for(int i=0;i<9;i++)
        for(int j=0;j<9;j++){
            int tmp = t[i][j];
            if(tmp == 0) continue;
            row[i][tmp] = true;
            col[j][tmp] = true;
            int b = 3*(i/3)+j/3;
            block[b][tmp] = true;
        }
}

pair<int,int> first(){
    pair<int,int> res;
    bool find = false;
    for(int i=0;i<9;i++){
        for(int j=0;j<9;j++){
            if(t[i][j]==0){
                res = {i,j};
                find = true;
                break;
            }
        }
        if(find) break;
    }
    return res;
}

pair<int,int> findNext(int x,int y){
    while(true){
        y++;
        if(y==9) {
            x++;
            y=0;
        }
        if(t[x][y]==0) return {x,y};
    }
}

bool check(){
    int sum=0;
    for(auto item:s){
        int x=item.X;
        int y=item.Y;
        sum+=t[x][y];
    }
    if(type) {
      if(sum>k) return true;
    }
    else {
      if(sum<k) return true;
    }
    return false;
}

void output(){
   int n=9;
   printf("[");
   for(int i=0;i<n;i++){
        printf("[");
        for(int j=0;j<n;j++){
            printf("%d",t[i][j]);
            if(j!=n-1) printf(",");
        }
        printf("]");
        if(i!=n-1) printf(",");
   }
   printf("]\n");
}
void output1(){
   int n=9;
   for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            printf("%d",t[i][j]);
            if(j!=n-1) printf(" ");
        }
        printf("\n");
   }
}

void go(int x,int y,int rest){
    int b = 3*(x/3)+y/3;
    for(int i=1;i<=9;i++){
        if(row[x][i]||col[y][i]||block[b][i]) continue;
        row[x][i] = col[y][i] = block[b][i] = true;
        t[x][y] = i;
        if(rest>1){
            pair<int,int>g = findNext(x,y);
            go(g.X,g.Y,rest-1);
            if(ok) return;
        }
        else{
            ok = check();
            if(ok) {
                output();
                return;
            }
        }
        t[x][y] = 0;
        row[x][i] = col[y][i] = block[b][i] = false;
    }
}

void solve(){
    ok = false;
    parse();
    pair<int,int> a = first();
    int cnt=0;
    for(int i=0;i<9;i++)
        for(int j=0;j<9;j++)
            if(t[i][j]==0) cnt++;
    go(a.X,a.Y,cnt);
}

int main(){
    for(int i=0;i<9;i++)
        for(int j=0;j<9;j++)
            scanf("%d",&t[i][j]);
    scanf("%d%d%d",&k,&n,&type);
    for(int i=0;i<n;i++){
        int a,b;
        scanf("%d%d",&a,&b);
        s.push_back({a-1,b-1});
    }
    solve();
}
