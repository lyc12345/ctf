from pwn import *
import time
r = remote('pwnable.kr',9016)

r.recv()
r.send("\n")
r.recv()
r.send("\n")

for now in xrange(1,101):
    print r.recvuntil('Stage '+str(now)+'\n')
    print r.recvline()
    d = ""
    for i in xrange(9):
        s = r.recvline()[1:-2]
        s = s.replace(',','')
        d += s
        d += '\n'
    print r.recvuntil('- additional rule -\n')
    t = 0
    tmp = r.recvline()
    print tmp
    if 'bigger' in tmp: t=1
    k = int(tmp.split()[-1])
    q = ""
    cnt = 0
    while True:
        tmp = r.recvline()
        print tmp
        if tmp[0]=='s': break
        cnt += 1
        tt = tmp.split()[-1][1:-1]
        q += tt.replace(',',' ')
        q += '\n'
    d += str(k)
    d += '\n'
    d += str(cnt)
    d += '\n'
    d += str(t)
    d += '\n'
    d += q
    p = process('./solve')
    print d
    p.send(d)
    res =  p.recvall()
    print res
    r.send(res)
r.interactive()
