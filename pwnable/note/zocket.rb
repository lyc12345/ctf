require 'socket'
require 'logger'
#$logger = Logger.new('payload')
#$logger.formatter = proc{|a,b,c,d|d}
class Zocket
  attr_reader :sock
  def initialize(host, port, logger: nil, timeout: 1)
    @sock = TCPSocket.new(host, port)
    @sock.binmode
    @logger = logger || $stdout
    @logger.binmode
    @timeout = timeout
  end

  def logger_mode(mode)
    if @logger.respond_to?(:setmode)
      @logger.setmode(mode)
    end
  end

  def write(s, do_log: true)
    @sock.write(s)
# $logger.info(s)
    logger_mode(:send)
    @logger.write(s) if do_log
  end

  def read(n, do_log: true)
    s = @sock.read(n)
    logger_mode(:recv)
    @logger.write(s) if do_log
    s
  end

  def read_until_timeout(timeout_sec = @timeout, do_log: true)
    s = ''
    loop do
      start_time = Time.now
      rs, = IO.select([@sock], [], [], timeout_sec)
      timeout_sec -= Time.now - start_time
      break if rs.nil?
      begin
        _s = @sock.readpartial(1024)
        logger_mode(:recv)
        @logger.write(_s) if do_log
        s << _s
      rescue EOFError
        break
      end
      break if timeout_sec < 0
    end
    s
  end

  def puts(s, do_log: true)
    s = s.to_s unless s.is_a?(String)
    s += "\n"
    write(s, do_log: do_log)
  end

  def gets(sep = $/, do_log: true)
    s = @sock.gets(sep)
    logger_mode(:recv)
    @logger.write(s) if do_log
    s
  end

  def interact(do_log: true)
    until @sock.closed? do
      rs, = IO.select([$stdin, @sock])
      if rs.include?($stdin)
        s = $stdin.readpartial(1024)
        @sock.write(s)
      end
      if rs.include?(@sock)
        s = @sock.readpartial(1024)
        $stdout.write(s)
        logger_mode(:recv)
        @logger.write(s) if @logger != $stdout && do_log
      end
    end
  end
end

class HexLogger
  def initialize(width = 24)
    @width = width
    @mode = nil
  end

  def setmode(mode)
    @mode = mode
  end

  def write(s)
    puts "#{@mode} (#{s.length} bytes):"
    w = @width * 3
    s.each_byte.each_slice(@width) do |line|
      hex = line.map{|b| '%02x' % b}.join(' ')
      hex << ' ' * (w - hex.length)
      str = line.map{|b| if b >= 32 && b <= 126 then b.chr else '.' end}.join
      puts "  #{hex} #{str}"
    end
  end

  def binmode
  end
end
