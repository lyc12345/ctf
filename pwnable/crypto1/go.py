from pwn import *
import re
import hashlib
def leak(ID,PW):
    r = remote("pwnable.kr","9006")
    r.recvuntil('ID')
    r.sendline(ID)
    r.recvuntil('PW')
    r.sendline(PW)
    print r.recv()
    #res = r.recvuntil(")").strip()
    #matcher = re.match(".*\((.*)\).*",res)
    #return matcher.group(1)
"""
flag = ""
for i in xrange(13):
    ans = leak("a"*16,"a"*(13-i))[32:64]
    for j in '1234567890abcdefghijklmnopqrstuvwxyz-_':
        b = "a"*(13-i)+'-'+ flag+j
        if leak("a"*16,b)[32:64] == ans:
            flag += j
            print flag
            break
print flag
"""
"""
flag = "you_will_neve"
for i in xrange(16):
    ans = leak("a","a"*(15-i))[32:64]
    for j in 'abcdefghijklmnopqrstuvwxyz-_1234567890':
        b = "a"*(15-i)+'-'+ flag+j
        if leak("a",b)[32:64] == ans:
            flag += j
            print flag
            break
print flag
"""
"""
flag = "you_will_never_guess_this_sug"
for i in xrange(16):
    ans = leak("a","a"*(15-i))[64:96]
    for j in 'abcdefghijklmnopqrstuvwxyz-_1234567890':
        b = "a"*(15-i)+'-'+ flag+j
        if leak("a",b)[64:96] == ans:
            flag += j
            print flag
            break
print flag
"""
"""
flag = "you_will_never_guess_this_sugar_honey_salt_co"
for i in xrange(16):
    ans = leak("a","a"*(15-i))[96:128]
    for j in 'abcdefghijklmnopqrstuvwxyz-_1234567890':
        b = "a"*(15-i)+'-'+ flag+j
        if leak("a",b)[96:128] == ans:
            flag += j
            print flag
            break
print flag
"""
flag = "you_will_never_guess_this_sugar_honey_salt_cookie"
print hashlib.sha256("admin"+flag).hexdigest()
r = remote("pwnable.kr","9006")
r.interactive()
