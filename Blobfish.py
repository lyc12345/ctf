from hashlib import md5
from z3 import *
s = [59, 21, 25, 32, 30, 3, 63, 38, 5, 29, 40, 53, 17, 56, 58, 37, 45, 43, 52, 61, 7, 55, 57, 12, 26, 13, 49, 16, 36, 8, 31, 41, 20, 51, 33, 15, 1, 0, 23, 27, 35, 18, 47, 62, 14, 60, 10, 54, 46, 50, 9, 48, 24, 28, 44, 2, 6, 34, 19, 42, 11, 39, 22, 4]
p = [14, 7, 25, 2, 22, 32, 29, 0, 28, 31, 6, 18, 12, 27, 33, 9, 17, 21, 8, 26, 23, 35, 4, 5, 11, 20, 30, 24, 15, 1, 19, 16, 10, 13, 34, 3]

rounds = 3
nsbox = 6	# number of s-boxes
bsbox = 6	# input bits per s-box
ssize = 1 << bsbox
bits = nsbox * bsbox
insize = 1 << bits

#####################################################
"""
import sys
sys.path.append('../stuff')
from secret import master_key as key
assert len(key) == 3
assert all(0 <= k < insize for k in key)
"""
#####################################################

_s_inv = [0] * len(s)
for i in range(len(s)):
	_s_inv[s[i]] = i

_p_inv = [0] * len(p)
for i in range(len(p)):
	_p_inv[p[i]] = i


def sbox(x):
	return s[x]


def pbox(x):
	y = 0
	for i in range(len(p)):
		y |= ((x >> i) & 1) << p[i]
	return y


def inv_s(x):
	return _s_inv[x]


def inv_p(x):
	y = 0
	for i in range(len(p)):
		y |= ((x >> p[i]) & 1) << i
	return y


def split(x): #1-1 
	y = [0] * nsbox
	for i in range(nsbox):
		y[i] = (x >> (i * bsbox)) & 0x3f
	return y


def merge(x): #1-1 split inverse
	y = 0
	for i in range(nsbox):
		y |= x[i] << (i * bsbox)
	return y


def round(p, k):
	u = [0] * nsbox
	x = split(p)
	for i in range(nsbox):
		u[i] = sbox(x[i])
	v = pbox(merge(u))
	w = v ^ k
	return w


def encrypt(p, rounds,key):
	for i in range(rounds):
		p = round(p, key[i])
	return p


def make_flag(k):
	param = md5(b"%x%x%x" % (k[0], k[1], k[2])).hexdigest()
	return "SharifCTF{%s}" % param

def init(p):
	u = [0] * nsbox
	x = split(p)
	for i in range(nsbox):
		u[i] = sbox(x[i])
	v = pbox(merge(u))
	return v

if __name__ == '__main__':
	#print(make_flag(key))
	f = open('log.txt').readlines()
	enc = []
	pla = []
	for line in f:
		line = line.strip().split()
		p1 = int(line[0][2:],16)
		e1 = int(line[1][2:],16)
		enc.append(e1)
		pla.append(init(p1))
	
	k1 = merge([20,41,60,36,49,32])
	npla = [init(i^k1) for i in pla]

	k2 = merge([5,37,30,57,16,54])
	nnpla = [init(i^k2) for i in npla]
	
	k3 = nnpla[0]^enc[0]
	for i in xrange(1,len(enc)): 
		assert k3 == nnpla[i]^enc[i]
	#print k3
	key = [k1,k2,k3]
	print make_flag(key)
	#SharifCTF{8aae67ec50080a359fc92beca22c52e3}
	"""
	xorenc = []	
	for i in xrange(1,len(enc)):
		xorenc.append(inv_p(enc[0]^enc[i]))
	for i in xrange(6):
		for g in xrange(64):
			check = True
			for j in xrange(len(enc)-1):
				v1 = sbox(split(npla[0])[i]^g)
				v2 = sbox(split(npla[j+1])[i]^g)
				v3 = split(xorenc[j])[i]
				#print v1,v2,v3
				if v1^v2 != v3:
					check = False
					break
			if check:
				print i,g
	"""
	"""
	xorenc = []
	for i in xrange(1,len(enc)):
		xorenc.append(inv_p(enc[0]^enc[i]))
	
	data1 = []
	for i in xrange(len(enc)): data1.append(split(pla[i])[5])
	#print data1
	debug = 0
	maybe = []
	for k1 in xrange(64):
		nd1 = [sbox(i^k1) for i in data1]#mapping = [19, 16, 10, 13, 34, 3] 0-5 6-11 12-17 18-23 24-29 30-35
		for k2 in xrange(2):
			aa,bb,cc = 5,3,0#4,4,5#3,1,2#2,4,1#1,4,2#0,1,3

			tnd1 = [((i >> aa)&1)^k2 for i in nd1]
			first = []			
			for i in xrange(64):
				if (i>>bb)&1 == tnd1[0]: first.append(i)
			ok = True
			if debug>0:
				print tnd1[0]
				print first,len(first)

			for i in xrange(0,len(enc)-1):
				second = []
				for j in xrange(64):
					if (j>>bb)&1 == tnd1[i+1]: second.append(j)
				has = False
				for f in first:
					for ss in second:
						if sbox(f)^sbox(ss) == split(xorenc[i])[cc]:
							has = True
							break
					if has == True: break
				if debug: 
					print first,"QQ"
					print second
					print split(xorenc[i])[1]
					print has
					
				if has == False:
					ok = False
					break
			if debug>0: debug-=1 
			if ok: maybe.append(k1)
	print maybe
	"""
#k1 = [20,41,60,36,49,32]
