#include <sys/ptrace.h>
#include <sys/reg.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/user.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

int do_child(int argc, char **argv);
int do_trace(pid_t child);
int wait_for_syscall(pid_t child);
void dump(uint64_t code_addr, pid_t child);

int main(int argc, char **argv) {
    if (argc < 2) {
        fprintf(stderr, "Usage: %s prog args\n", argv[0]);
        exit(1);
    }

    pid_t child = fork();
    if (child == 0) {
        return do_child(argc-1, argv+1);
    } else {
        return do_trace(child);
    }
}

int do_child(int argc, char **argv) {
    char *args [argc+1];
    memcpy(args, argv, argc * sizeof(char*));
    args[argc] = NULL;
    ptrace(PTRACE_TRACEME);
    kill(getpid(), SIGSTOP);
    return execvp(args[0], args);
} 

char* read8(uint64_t addr, pid_t child) {
	char *buf = malloc(8);
	unsigned long long tmp = ptrace(PTRACE_PEEKDATA, child, addr);
	memcpy(buf, &tmp, sizeof tmp);
	return buf;
}

void dump(uint64_t code_addr, pid_t child) {
	//fprintf(stderr,"0x%lx\n",code_addr-(0x7f0000-0x5ec000));
	uint64_t addr = code_addr;
	uint64_t start;
	fprintf(stderr,"dump!\n");
	while(1) {
		char *buf = read8(addr,child);
		//fprintf(stderr,"0x%lx %8s\n",addr,buf);
		//gets(buf);
		if(strncmp(buf+1,"ELF",3) == 0){
			start = addr;
			free(buf);
			break;
		}
		free(buf);
		addr -= 0x1000;
	}
	fprintf(stderr,"start: 0x%lx\n",start);

	uint64_t diff = code_addr - start;
	addr = start;
	fprintf(stderr,"%llu\n",(unsigned long long)diff);
	FILE *out = fopen("dump","wb");
	while(addr<code_addr) {
		char *tmp = read8(addr,child);
		addr += 8;
		fwrite(tmp,1,8,out);
	}
	fclose(out);
	return;
}


int do_trace(pid_t child) {
    int status, syscall, retval,heap;
    uint64_t code_addr;
    waitpid(child, &status, 0);
    ptrace(PTRACE_SETOPTIONS, child, 0, PTRACE_O_TRACESYSGOOD);
    while(1) {
    	if (wait_for_syscall(child) != 0) break;
	syscall = ptrace(PTRACE_PEEKUSER, child, sizeof(long)*ORIG_RAX);
       	//fprintf(stderr, "syscall(%d) = ", syscall);
        if (syscall == 10) {
		struct user_regs_struct regs;
		ptrace(PTRACE_GETREGS, child, NULL, &regs );
		//fprintf(stderr,"mprotect rdi = 0x%lx\n", regs.rdi );
		if(regs.rdi <0x660000000000) {
			code_addr = regs.rdi;
			fprintf(stderr,"code pos: 0x%lx\n",code_addr);
			dump(code_addr,child);
			break;
		}
	}
	if (wait_for_syscall(child) != 0) break;
	retval = ptrace(PTRACE_PEEKUSER, child, sizeof(long)*RAX);
    }
    return 0;
}

int wait_for_syscall(pid_t child) {
    int status;
    while (1) {
        ptrace(PTRACE_SYSCALL, child, 0, 0);
	waitpid(child, &status, 0);
	if (WIFSTOPPED(status) && WSTOPSIG(status) & 0x80)
            return 0;
	if (WIFEXITED(status))
            return 1;
    }
}




