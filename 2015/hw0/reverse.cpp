#include<bits/stdc++.h>
typedef unsigned char uc;

void myxor(int i,uc *x){
		for(int j=0;j<4;j++) x[i+j]^=~x[i+j+4];
}
void decrypt(int i,uc *x){
		uc y[4];
		y[1]=x[i]^x[i+2];
		y[3]=y[1]^x[i+1];
		y[0]=y[1]^x[i+3];
		y[2]=y[0]^x[i+2];
		for(int j=0;j<4;j++) x[i+j]=y[j];
}
uc y[1000];
int main(){
		FILE *file = fopen("text.enc","rb");
		if(file != NULL){
				int len = 0;
				fseek(file,0,SEEK_END);
				len = ftell(file);
				fseek(file,0,SEEK_SET);
				uc *x = (uc*)malloc(sizeof(uc)*len);
				fread(x,len,1,file);
				//for(int i=0;i<len;i++) y[i]=x[i];
				for(int i=len-4;i>=0;i-=4){
						if(i!=len-4) myxor(i,x);
						decrypt(i,x);
				}
				//for(int i=0;i<len;i++) printf("%d %d\n",y[i],x[i]);
				printf("%s\n",x);
				fclose(file);
		}
}
