import socket
import base64
import zlib
def parse2(cipher):
    ans=""
    for i in range(0,len(cipher)):
        c = ord(cipher[i])
        if c>=ord('A') and c-7<ord('A'):
            c = ord('z')-(ord('A')-(c-6))
        elif c>=ord('a') and c-7<ord('a'):
            c = ord('9')-(ord('a')-(c-6))
        elif c>=ord('0') and c-7<ord('0'):
            c = ord('Z')-(ord('0')-(c-6))
        else:
            c = c-7
        ans+=(chr(c))
    return ans

def parse3(cipher):
    ans=""
    for i in range(0,len(cipher)):
        j=i+1
        j=j%62
        c = ord(cipher[i])
        if c>=ord('a') and c<=ord('z'):
            c = c-ord('a')+10
        elif c>=ord('A') and c<=ord('Z'):
            c = c-ord('A')+36
        elif c>=ord('0') and c<=ord('9'):
            c = c-ord('0')
        c=c-j
        if c<0:
            c = c+62
        if c>=10 and c<=35:
            c=c-10+ord('a')
        elif c>=36 and c<=61:
            c=c-36+ord('A')
        elif c>=0 and c<=9:
            c=c+ord('0')
        ans+=(chr(c))
    return ans

def parse4(cipher):
    ans=""
    mapping = "VHJigwGMmFYzxcvPDkK8CayR6LrlN7utjd0S5qp1EX2nT34eWQhoBZfAsbU9IOB"
    for i in range(0,len(cipher)):
        c=-1
        for j in range(0,len(mapping)):
            if mapping[j] == cipher[i]:
                c=j
                break
        if c>=26 and c<=51:
            c=c-26+ord('a')
        elif c>=0 and c<=25:
            c=c+ord('A')
        elif c>=51 and c<=61:
            c=c+ord('0')-51
        ans+=(chr(c))
    return ans

def parse5(cipher):
    ans=""
    mapping="0ffb41ggc52hhd63iie74jjf85kkg96llha7mmib8nnjc9ookdapplebqqmfcrrngdssohettpifuuqjgvvrkhwwslixxtmjyyunkzzvolAAwpmBBxqnCCyroDDzspEEAtAPPLEBQQMFCRRNGDSSOHETTPIFUUQJGVVRKHWWSLIXXTMJYYUNKZZVOL00WPM11XQN22YRO33ZSP440TQ551UR662VS773WT884XU995YVaa6ZWbb70Xcc81Ydd92Zeea3qFFBurGGCvsHHDwtIIExuJJFyvKKGzwLLHAxMMIByNNJCzOOKD"
    print cipher
    for i in range(0,len(cipher)):
        j=i%5;
        c=-1
        for j in range(j,len(mapping),5):
            c=c+1
            if mapping[j] == cipher[i]:
                break
        if c>=26 and c<=51:
            c=c-26+ord('a')
        elif c>=0 and c<=25:
            c=c+ord('A')
        elif c>=52 and c<=61:
            c=c+ord('0')-52
        ans+=(chr(c))
    return ans


def parse6(cipher):
    ans=""
    for i in range(0,len(cipher)):
        if i==0:
            ans+=cipher[i]
        else:
            add=ord(cipher[i-1])
            if add>=ord('a') and add<=ord('z'):
                add = add-ord('a')
            elif add>=ord('A') and add<=ord('Z'):
                add = add-ord('A')+26
            elif add>=ord('0') and add<=ord('9'):
                add = add-ord('0')+52
            c=ord(cipher[i])
            if c>=ord('a') and c<=ord('z'):
                c = c-ord('a')
            elif c>=ord('A') and c<=ord('Z'):
                c = c-ord('A')+26
            elif c>=ord('0') and c<=ord('9'):
                c = c-ord('0')+52
            add=add%62
            c=c-add
            if c<0:
                c=c+62
            if c>=26 and c<=51:
                c=c-26+ord('A')
            elif c>=0 and c<=25:
                c=c+ord('a')
            elif c>=52 and c<=61:
                c=c+ord('0')-52
            ans+=(chr(c))
    return ans

def parse7(cipher):
    ans = base64.b64decode(cipher)
    return ans

def calc(a):
    if ord(a)>=ord('a') and ord(a)<=ord('f'):
        return ord(a)-ord('a')+10
    else:
        return ord(a)-ord('0')

def parse8(cipher):
    f = open("a.in","wb")
    for i in range(0,len(cipher),2):
        c=calc(cipher[i])*16+calc(cipher[i+1])
        f.write(chr(c))
    f.close()
    f = open("a.in","r")
    data=zlib.decompress(f.read(), 16+zlib.MAX_WBITS)
    print data
    return data

def parse9(cipher):
    mapping = "kqmpsotjlunigeabfrcdh"
    ans=list(cipher)
    for i in range(0,len(cipher)):
        j=ord(mapping[i])-ord('a')
        ans[j]=cipher[i]
    return "".join(ans)

s = socket.socket(socket.AF_INET,socket.SOCK_STREAM,6)
s.connect(("csie.ctf.tw",10112))
p = False
cipher=""
num = 0
while True:
    buf = s.recv(1024)
    if buf == "":
        continue
    print buf
    sp = buf.split()
    l = len(sp)
    cipher = ""
    plain = ""
    for i in range(0,l):
        if sp[i] == "Ciphertext:":
            cipher = sp[i+1]
            num = num+1
            if num == 1:
                s.send(cipher+'\n')
            elif num == 2:
                s.send(parse2(cipher)+'\n')
            elif num == 3:
                s.send(parse3(cipher)+'\n')
            elif num == 4:
                s.send(parse4(cipher)+'\n')
            elif num == 5:
                s.send(parse5(cipher)+'\n')
            elif num == 6:
                s.send(parse6(cipher)+'\n')
            elif num == 6:
                s.send(parse6(cipher)+'\n')
            elif num == 7:
                s.send(parse7(cipher)+'\n')
            elif num == 8:
                s.send(parse8(cipher)+'\n')
            elif num == 9:
                s.send(parse9(cipher)+'\n')
