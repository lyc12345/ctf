#!/usr/bin/python
from pwn import *
import base64
import time
printable = string.printable
user = "123\n"
def connect(pwd):
    time.sleep(0.1)
    r = remote('csie.ctf.tw',10117)
    r.recv(4096)
    r.send(user)
    r.recv(4096)
    r.send(pwd+'\n')
    b = r.recv(4096).strip().split()[1]
    d = base64.b64decode(b)[:48]
    e = [ord(tmp) for tmp in d]
    r.close()
    return e
s = ", flag: "
flag = ""
end = False
for num in xrange(1,21):#
    now = "A"*(20-num)
    ans = connect(now)
    for i in printable:
        tmp = now+s+flag+i
        z = connect(tmp)
        if z == ans:
            flag += i
            if i == '}':
                end = True
            break
    if end == True:
        break
print "flag=",flag
