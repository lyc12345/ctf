#!/usr/bin/python
from pwn import *
import base64
import time

D = "NfHBHcVs1MzChTWg/yPibl97EcV9e566VfocKI60xhOVG/ko1PVQ2g9F5etdzLiAwPhczk8zPqJ99ohkKWehVg=="
D = base64.b64decode(D)

def connect(arg):
    ret = []
    time.sleep(0.1)
    r = remote('csie.ctf.tw',10118)
    for i in xrange(256):
        r.send(arg[i])
        ret.append(r.recvline().strip())
    r.close()
    return ret

flag = ""
for i in xrange(1,4):
    ans = [0]*16
    R = [0]*16
    for j in xrange(15,-1,-1):
        for k in xrange(16):
            R[k] = R[k]^(16-j)^(15-j)
        msg = []
        for k in xrange(256):
            R[j] = k
            msg.append(base64.b64encode(("".join(map(chr,R)))+D[16*i:16*(i+1)])+'\n')
        ret = connect(msg)
        for k in xrange(256):
            if ret[k] == 'true':
                R[j] = k
                break
        ans[j] = (16-j)^k^ord(D[16*(i-1)+j])
    flag += ("".join(chr(tmp) for tmp in ans))
print flag.strip()
