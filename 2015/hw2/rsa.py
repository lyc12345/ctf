#!/usr/bin/python
from pwn import *
import time
import sympy
r = remote('csie.ctf.tw',10120)
for t in xrange(0,11):
    r.recvline()
    n = int(r.recvline().strip())
    a = int(r.recvline().strip())
    b = int(r.recvline().strip())
    c = (b-a+2)%n
    d=(2*a+b-1)%n
    m = (d*sympy.invert(c,n))%n
    r.send(str(m)+'\n')
import sys
for c in  ('%x' % int(m)).decode('hex'):
    sys.stdout.write(c)
    if c =='}':
        break
