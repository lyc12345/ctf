#!/usr/bin/python
import socket
import time
import sympy
sc = socket.socket(socket.AF_INET, socket.SOCK_STREAM);
sc.connect(('csie.ctf.tw',10119))
c =  0x1c9d992fd1c6c9f26b24fb127e3e1ac343eadb0dad36c27747111ad07238bf9bc76d16737f8f7fc6752df563027fc6614a8c803de38bbe6aefca0e7f3ec739ba4
c = c*2
sc.send(str(c)+'\n')
ans = int(sc.recv(4096).strip())
sc2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM);
sc2.connect(('csie.ctf.tw',10119))
sc2.send("2\n")
two = sc2.recv(400).strip()
t = int(two)
n = 29483112906907846550407371381907804051925957834404110624325531950200215274674279351500117069061279396866776918114198748748643519779529947303729199772247349
ans = (ans*sympy.invert(t,n))%n
print hex(int(ans))[2:-1].decode('hex')
