#!/usr/bin/env python

from pwn import *
import time
import base64
import random
from Crypto.Cipher import AES
from Crypto.Hash import SHA256

def sha(message):
    sha256 = SHA256.new()
    sha256.update(message)
    digest = sha256.digest()
    output = message+'||'+base64.b64encode(digest)
    return output

flag = ""
def connect():
    r = remote('csie.ctf.tw',10121)
    r1 = remote('csie.ctf.tw',10121)
    r.send(sha("admin||"+str(100))+'\n')
    r.recvline()
    r.recvline()
    d = r.recvline()
    c = d.split()[11].split('||')
    Ns = c[0]
    r1.send(sha("admin||"+Ns)+'\n')
    r1.recvline()
    r1.recvline()
    e = r1.recvline()
    cipher = e.split()[11].split('||')[1]
    r1.close()
    print r.recv(4096)
    r.send(sha(cipher)+'\n')
    print r.recvline()
    print r.recvline()
    r.close()

connect()
print flag
