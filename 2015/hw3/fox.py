#!/usr/bin/python
from pwn import *
import base64
import time
import hashlib
import string

d = hashlib.sha256
b = base64.b64encode
printable = string.printable
x = "fox||848"
y = "thisIsServer||124"
z = "LoMnmPrYpK24twHlbWat+Zxg0sx5rSgUS09SriuT8+U="
flag = ""

def Z(s):
    ret = 0
    for i in xrange(14,17):
        ret = ret*10+ord(s[i])-48
    return ret

def connect():
    time.sleep(0.1)
    r = remote('csie.ctf.tw',10122)
    r.send(x+'\n')
    server = Z(r.recv(4096).strip())
    r.send(z+'\n')
    response = r.recv(4096).strip()
    response = r.recv(4096).strip()
    if server == 124:
        print "============================="
        print response
        print "============================="
    if response.startswith("Error"):
        print str(server)+",wrong"
    else: 
        print str(server)+",yes"
        print response
        flag = response
    r.close()

for t in xrange(1000):
    connect()
print flag
